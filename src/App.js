
import Products from './pages/products/index.js'
import './reset.css'

function App() {
  return (
      <div className="App">
        <Products /> 
      </div>
  );
}


export default App;
